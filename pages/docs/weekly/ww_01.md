---
Week: 6
Content: GIT, Gitlab, Jobs, Nyheder
Material: See links in weekly plan
Initials: NISI
---

# Uge 36 - Introduktion til faget og opsætning af værktøjer

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Forberedelse

- Læs undervisningsplanen og opgaver
- Udfyld spørgeskema om forudsætninger (link på itslearning og kræver login med UCL creds)
- Læs kapitel 1 – it-kriminalitet og trusler fra cyberspace, i “IT Sikkerhed i praksis” (se [boglisten](https://esdhweb.ucl.dk/D22-2146015.pdf))

### Praktiske mål

- Forudsætnings survey udfyldt
- VMware workstation installeret
- Kali Linux VM installeret på VMware
- Alle studerende har forbindelse til TryHackMe via Kali/VPN
- THM Linux fundamentals part 1+2+3

### Opgaver

1. [Opgave 4 - Studieordning og læringsmål](https://ucl-pba-its.gitlab.io/exercises/intro/4_intro_opgave_læringsmål)
2. [Opgave 24 - Kali Linux på VM Ware](https://ucl-pba-its.gitlab.io/exercises/intro/24_intro_opgave_vmware_kali/)
3. [Opgave 25 - TryHackMe](https://ucl-pba-its.gitlab.io/exercises/intro/25_intro_opgave_thm/)
4. [THM: Learning Cyber Security](https://tryhackme.com/room/beginnerpathintro)
5. [THM: Linux fundamentals part 1](https://tryhackme.com/room/linuxfundamentalspart1)
5. [THM: Linux fundamentals part 2](https://tryhackme.com/room/linuxfundamentalspart2)
5. [THM: Linux fundamentals part 3](https://tryhackme.com/room/linuxfundamentalspart3)

### Læringsmål

- Den studerende kender fagets læringsmål og fagets semester indhold
- Den studerende kan vurdere egne faglige evner i relation til fagets læringsmål
- Den studerende har viden om virtuelle maskiner
- Den studerende har viden om VPN opsætning

## Afleveringer

- Forudsætnings survey
- Præsentations emne

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer og links


